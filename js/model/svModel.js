function SinhVien(_ma, _ten, _email, _matKhau, _diemLy, _diemToan, _diemHoa) {
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.matKhau = _matKhau;
    this.diemToan = _diemToan;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.tinhDTB = function () {
        var dtb = (this.diemToan + this.diemLy + this.diemHoa) / 3;
        return dtb.toFixed(1);
    };
}
